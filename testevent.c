#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/uinput.h>
#include <libevdev/libevdev.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

int main(void){
    int err;
    struct libevdev *dev;
    struct libevdev_uinput *uidev;
    struct timespec start, end;
    
    dev = libevdev_new();
    libevdev_set_name(dev, "test device");
    libevdev_enable_event_type(dev, EV_KEY);
    libevdev_enable_event_code(dev, EV_KEY, LED_CAPSL, NULL);
    libevdev_enable_event_code(dev, EV_KEY, KEY_SPACE, NULL);
    
    int fd = open("/dev/uinput", O_RDWR | O_NONBLOCK);
    if (fd == -1)
        printf("cannot open dev/uinput \n", strerror(errno));
    err = libevdev_uinput_create_from_device(dev, fd, &uidev);
    if (err != 0)
        return err;
    
    // ... do something ...
    FILE *evtime3 = fopen("evtime3.txt", "w");
    if (evtime3 == NULL)
        {
            printf("Could not open file");
            return 0;
        } 
    char sysfs_device_name[16];
    double count;
    ioctl(fd, UI_GET_SYSNAME(sizeof(sysfs_device_name)), sysfs_device_name);
    fprintf(evtime3, "/sys/devices/virtual/input/%s\n", sysfs_device_name);

    //buy time to record event with libinput helper tools
    //sleep(10);
    for (int i=0; i<15; i++){
        //initialize clock
        clock_gettime(CLOCK_REALTIME, &start);

        for (int i=0; i<20; i++){
            /* Key press, report the event, send key release, and report again */
            libevdev_uinput_write_event(uidev, EV_KEY, KEY_SPACE, 1);
            libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
            libevdev_uinput_write_event(uidev, EV_KEY, KEY_SPACE, 0);
            libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
        }

        //time difference
        clock_gettime(CLOCK_REALTIME, &end);
        long second = end.tv_sec - start.tv_sec;
        long nsecond = end.tv_nsec - start.tv_nsec;
        double elapsed = second + nsecond*1e-9;
        fprintf(evtime3, "REAL time: %.10f seconds \n", elapsed);
        count += elapsed;
        sleep(5);
    }
    double avg = count/15;
    fprintf(evtime3, "Average %.10f seconds \n", avg);
    fclose(evtime3);
    
    libevdev_uinput_destroy(uidev);
    close(fd);

    return 0;
}