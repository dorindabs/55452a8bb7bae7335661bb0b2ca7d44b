CC=gcc
CFLAGS=-I.
DEPS = libevdev.h

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)
LDLIBS=-levdev

testevent: libevtestevent.o
	$(CC) $(LDLIBS) -o testevent libevtestevent.o
